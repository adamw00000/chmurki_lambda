import json
import sys
import logging
import rds_config
import ses_sender
import pymysql
import boto3

rds_host = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
    conn.autocommit(True)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
client = boto3.client('cognito-idp')

def getEmail(userId):
    userAttrs = client.admin_get_user(UserPoolId='eu-central-1_e1Zc6rieZ', Username=userId)['UserAttributes']
    return next(attr['Value'] for attr in userAttrs if attr['Name'] == 'email')

def lambda_handler(event, context):
    logger.info(event)
    
    eventId = event['pathParameters']['eventId']
    userId = event['requestContext']['authorizer']['claims']['sub']
    
    try:
        with conn.cursor() as cur:
            selectCurrentPlayersQuery = """
            SELECT COUNT(*) FROM `EventUsers`
            WHERE EventId = %s
            GROUP BY EventId
            """
            cur.execute(selectCurrentPlayersQuery, (eventId))
            currentPlayers = cur.fetchone()[0]
            print(currentPlayers)
            
            selectPlayersBoundsQuery = """
            SELECT MaxPlayers, MinPlayers FROM `Events`
            WHERE Id = %s
            """
            cur.execute(selectPlayersBoundsQuery, (eventId))
            result = cur.fetchone()
            maxPlayers = result[0]
            minPlayers = result[1]
            print(result)
            
            if maxPlayers != None and currentPlayers >= maxPlayers:
                return {
                    'statusCode': 400,
                    'headers': {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": 'PUT',
                        "Access-Control-Allow-Credentials": True
                    },
                    'body': json.dumps('Joining event failed, max number of players reached')
                }
            
            selectUserQuery = """
            SELECT * FROM `EventUsers`
            WHERE EventId = %s AND UserId = %s
            """
            cur.execute(selectUserQuery, (eventId, userId))
            euPair = cur.fetchall()
            print(euPair)
            if (len(euPair) != 0):
                return {
                    'statusCode': 400,
                    'headers': {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": 'PUT',
                        "Access-Control-Allow-Credentials": True
                    },
                    'body': json.dumps('Joining event failed, user already joined the event')
                }
            
            insertUserQuery = """
            INSERT INTO `EventUsers` (EventId, UserId)
            VALUES (%s, %s)
            """
            cur.execute(insertUserQuery, (eventId, userId))
            
            selectEmailInfoQuery = """
            SELECT CreatorId, Title FROM `Events`
            WHERE Id = %s
            """
            cur.execute(selectEmailInfoQuery, (eventId))
            emailInfo = cur.fetchone()
            print(emailInfo)
            creatorId = emailInfo[0]
            title = emailInfo[1]
            print(emailInfo)


            try:
                userEmail = getEmail(creatorId)
                print(userEmail)
                logger.info(userEmail)

                if (currentPlayers + 1 == minPlayers):
                    print('ATTEMPTING TO SEND AN EMAIL')
                    try:
                        result = ses_sender.send_min_players_reached_email(userEmail, title)
                        print(result)
                    except:
                        print('User is not verified')
                else:
                    print('DID NOT ATTEMPT TO SEND EMAIL')
            except:
                print('User doesn\'t exist')

            conn.commit()
            
    except pymysql.Error as e:
        return {
            'statusCode': 500,
            'headers': {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": 'PUT',
                "Access-Control-Allow-Credentials": True
            },
            'body': json.dumps('Joining event failed, error %d: %s' %(e.args[0], e.args[1]))
        }
        
    # TODO implement
    return {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": 'PUT',
            "Access-Control-Allow-Credentials": True
        },
        'body': json.dumps('Joining event succeeded')
    }