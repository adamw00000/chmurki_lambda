import json
import sys
import logging
import rds_config
import pymysql

rds_host = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
    conn.autocommit(True)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
    
def lambda_handler(event, context):
    logger.info(event)
    creatorId = event['requestContext']['authorizer']['claims']['sub']

    try:
        with conn.cursor() as cur:
            selectEventQuery = """
            SELECT * FROM `Events`
            WHERE `CreatorId` = %s
            """
            cur.execute(selectEventQuery, (creatorId,))
            events = cur.fetchall()
            
            print(events)
            result = []
            for event in events:
                selectBGQuery = """
                SELECT BoardGameId 
                FROM `EventBoardGames`
                WHERE EventId = %s
                """
                cur.execute(selectBGQuery, (event[0]))
                boardGames = cur.fetchall()
                
                bgList = []
                for bg in boardGames:
                    bgList.append(bg[0])
                    
                selectPlayersQuery = """
                SELECT COUNT(*) FROM `EventUsers`
                WHERE EventId = %s
                GROUP BY EventId
                """
                cur.execute(selectPlayersQuery, (event[0]))
                currentPlayers = cur.fetchone()

                # selectTagsQuery = """
                # SELECT t.Id, t.Tag FROM `EventTags` et
                # JOIN `Tags` t ON et.TagId = t.Id AND et.EventId = %s
                # """
                # cur.execute(selectTagsQuery, (eventId))
                # tags = cur.fetchall()
                
                # tagList = []
                # for t in tags:
                #     d = {}
                #     d["id"] = t[0]
                #     d["tag"] = t[1]
                #     tagList.append(d)
                
                result.append({
                    "id": event[0],
                    "creatorId": event[1],
                    "title": event[2],
                    "city": event[3],
                    "date": event[4],
                    "startTime": event[5],
                    "duration": event[6],
                    "description": event[7],
                    "photo": event[8],
                    "minPlayers": event[9],
                    "maxPlayers": event[10],
                    "locked": event[11],
                    "chatURL": event[12],
                    "boardGames": bgList,
                    "tags": [ "tmp" ],
                    "currentPlayers": currentPlayers[0]
                    })

    except pymysql.Error as e:
        print(e.args)
        return {
            'statusCode': 500,
            'headers': {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": 'GET',
                "Access-Control-Allow-Credentials": True
            }
        }
        
    # TODO implement
    return {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": 'GET',
            "Access-Control-Allow-Credentials": True
        },
        'body': json.dumps({"events": result}, default=str)
    }