import json
import sys
import logging
import rds_config
import pymysql
import ses_sender
import boto3
import shortuuid
import time

rds_host = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

client = boto3.client('cognito-idp')

try:
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
    conn.autocommit(True)
except:
    logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")

def getEmail(userId):
    userAttrs = client.admin_get_user(UserPoolId='eu-central-1_e1Zc6rieZ', Username=userId)['UserAttributes']
    return next(attr['Value'] for attr in userAttrs if attr['Name'] == 'email')
    
def lambda_handler(event, context):
    logger.info(event)
    
    eventId = event['pathParameters']['eventId']
    userId = event['requestContext']['authorizer']['claims']['sub']
    
    try:
        with conn.cursor() as cur:
            selectCreatorQuery = """
            SELECT CreatorId, MinPlayers, Locked, Title FROM `Events`
            WHERE Id = %s
            """
            cur.execute(selectCreatorQuery, (eventId))
            eventInfo = cur.fetchone()
            print(eventInfo)
            creatorId = eventInfo[0]
            minPlayers = eventInfo[1]
            locked = eventInfo[2]
            eventTitle = eventInfo[3]
            
            selectPlayersQuery = """
            SELECT COUNT(*) FROM `EventUsers`
            WHERE EventId = %s
            GROUP BY EventId
            """
            cur.execute(selectPlayersQuery, (eventId))
            currentPlayers = cur.fetchone()[0]
            
            if (creatorId != userId or currentPlayers < minPlayers or locked == 1):
                return {
                    'statusCode': 400,
                    'headers': {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": 'PUT',
                        "Access-Control-Allow-Credentials": True
                    },
                    'body': json.dumps('Locking event failed, you are not an event creator, '
                                       'minimal player number was not reached or event was already locked in')
                }
            
            updateLockedQuery = """
            UPDATE `Events` SET Locked = 1, ChatURL = %s
            WHERE Id = %s
            """
            chatURL = str(eventId) + '-' + str(shortuuid.uuid())
            cur.execute(updateLockedQuery, (chatURL, eventId))

            getParticiantsQuery = """
            SELECT UserId FROM `EventUsers`
            WHERE EventId = %s AND UserId != %s
            """
            cur.execute(getParticiantsQuery, (eventId, creatorId))
            participants = cur.fetchall()
            for participant in participants:
                try:
                    userEmail = getEmail(participant[0])
                    print(userEmail)
                    print('ATTEMPTING TO SEND AN EMAIL')
                    try:
                        result = ses_sender.send_lock_email(userEmail, eventTitle)
                        print(result)
                    except:
                        print('User is not verified')
                except:
                    print('User doesn\'t exist')
                time.sleep(1)
            
            conn.commit()
            
    except pymysql.Error as e:
        return {
            'statusCode': 500,
            'headers': {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": 'PUT',
                "Access-Control-Allow-Credentials": True
            },
            'body': json.dumps('Locking event failed, error %d: %s' %(e.args[0], e.args[1]))
        }
        
    # TODO implement
    return {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": 'PUT',
            "Access-Control-Allow-Credentials": True
        },
        'body': json.dumps('Locking event succeeded')
    }