from __future__ import print_function
import boto3
from botocore.exceptions import ClientError
 
SENDER = ""
RECIPIENT = ""
SUBJECT = ""
 
AWS_REGION = "eu-west-1"
 
 
# The character encoding for the email.
CHARSET = "UTF-8"
 
# Create a new SES resource and specify a region.
client = boto3.client('ses',region_name=AWS_REGION)
 
# Try to send the email.
def send_min_players_reached_email(targetEmail, eventTitle):
    global RECIPIENT,SENDER,SUBJECT
    
    RECIPIENT = targetEmail
    SENDER = "Chmurki Team <adam.wawrzenczyk@o2.pl>"
    
    SUBJECT = "Minimum player count reached for an event: %s" % eventTitle
    
    BODY_TEXT = ("Minimal player count reached!\r\n"
                "You can now lock the event using the website and start discussing event details!"
                )
    BODY_HTML = """
    <html>
    <head>

    </head>
    <body>
        <h1>Minimal player count reached!</h1>
        <p>
            You can now lock the event using the website and start discussing event details!
        </p>
    </body>
    </html>
    """

    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        return e.response['Error']['Message']
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
        return "Email sent! Message ID:" + str(response['MessageId'])

def send_lock_email(targetEmail, eventTitle):
    global RECIPIENT,SENDER,SUBJECT
    
    RECIPIENT = targetEmail
    SENDER = "Chmurki Team <adam.wawrzenczyk@o2.pl>"
    
    SUBJECT = "Event creator locked in an event that you joined: %s" % eventTitle
    
    BODY_TEXT = ("Event has been locked in!\r\n"
                "A chat on the on the website is now unlocked - you can jump in and start discussing event details right now!"
                )
    BODY_HTML = """
    <html>
    <head>

    </head>
    <body>
        <h1>Event has been locked in!</h1>
        <p>
            A chat on the on the website is now unlocked - you can jump in and start discussing event details right now!
        </p>
    </body>
    </html>
    """

    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        return e.response['Error']['Message']
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
        return "Email sent! Message ID:" + str(response['MessageId'])